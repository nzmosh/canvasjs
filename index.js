(function () { 'use strict';

var canvas;
var ctx;
var canvas_width = window.innerWidth * 2;
var canvas_height = window.innerHeight * 2;
var a = 1 / 1000;
var g = 1 / 300;
var dots = [];
var distance = 50;
var pane;

var params = {
  'num': 500,
  'speed': 10,
  'color': 'white',
  'distance': 50,
  'radius': 3
};

function init () {
  canvas = document.getElementById('canvas');
  ctx = canvas.getContext('2d');

  pane = new Tweakpane();
  pane.addControl(params, 'num');
  pane.addControl(params, 'speed');
  pane.addControl(params, 'distance');
  //pane.addControl(params, 'color');
  pane.addControl(params, 'radius');

  window.requestAnimationFrame(draw);
}

function create () {
  var num = params.num - dots.length;
  if (num > 0) {
    for (var i = 0; i < num; i++) {
      var deg = 360 * Math.random();
      var aa = a * params.speed * .5 + a * params.speed * .5 * Math.random();
      dots.push({
        x: canvas_width * Math.random(),
        y: canvas_height * Math.random(),
        deg: deg,
        a: aa * params.speed,
        vx: aa * params.speed * Math.sin(deg / 180 * Math.PI),
        vy: aa * params.speed * Math.cos(deg / 180 * Math.PI)
      });
    }
  } else {
    dots.slice(-num);
  }
}

function draw () {
  create();
  canvas_width = window.innerWidth * 2;
  canvas_height = window.innerHeight * 2;
  canvas.setAttribute('width', canvas_width);
  canvas.setAttribute('height', canvas_height);

  ctx.fillStyle = 'black';
  ctx.fillRect(0, 0, canvas_width, canvas_height);

  ctx.strokeStyle = 'dimgray';
  ctx.fillStyle = params.color;

  for (var i = 0; i < params.num; i++) {
    //if (i === 0) {
    //  ctx.fillStyle = 'magenta';
    //} else {
    //  ctx.fillStyle = params.color;
    //}
    var dot = dots[i]
    dot.x = (dot.x + dot.vx + canvas_width) % canvas_width;
    dot.y =  (dot.y + dot.vy + canvas_height) % canvas_height;
    //dot.deg = dot.deg + (Math.random()*10 - 5);
    dot.vx = dot.a * params.speed * Math.sin(dot.deg / 180 * Math.PI);
    dot.vy = dot.a * params.speed * Math.cos(dot.deg / 180 * Math.PI);

    //ctx.beginPath();
    //ctx.moveTo(dot.x, dot.y);
    //ctx.lineTo(dot.x + dot.vx * 30, dot.y + dot.vy * 30);
    //ctx.stroke();

    ctx.beginPath();
    ctx.arc(dot.x, dot.y, params.radius, Math.PI*2, false);
    ctx.fill();
  }

  ctx.strokeStyle = 'gray';
  ctx.beginPath();
  for (var i = 0; i < params.num; i++) {
    for (var j = i + 1; j < params.num; j++) {
      var a = dots[i];
      var b = dots[j];
      var d = Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
      if (d < params.distance) {
        ctx.moveTo(a.x, a.y);
        ctx.lineTo(b.x, b.y);
        var gg = (params.distance - d) / params.distance * g;
        var deg = distanceToDeg(a.x - b.x, a.y - b.y) % 360;
        a.deg = (a.deg + (a.deg - (deg - 360)) * gg) % 360;
        b.deg = (b.deg + (b.deg - deg) * gg) % 360;
        //if (i === 0) { console.log(a.deg, deg); }
        //var bdeg = b.deg;
        //a.deg += (a.deg - b.deg) * (d/params.distance);
        //b.deg += (b.deg - a.deg) * (d/params.distance);
      }
      //console.log(d);
    }
  }
  ctx.stroke();

  window.requestAnimationFrame(draw);
}

function distanceToDeg (x, y) {
  var calculate_cosine = function (x, y) { return x / Math.sqrt(x*x + y*y); };
  var calculate_radian = function (cos) { return Math.acos(cos) / (Math.PI / 180); };
  var rad = calculate_radian(calculate_cosine(x, y));
  var deg = rad * 180 / Math.PI;
  return deg;
}

window.getDots = function () {
  return dots;
}

window.addEventListener('DOMContentLoaded', init, false);

})();
